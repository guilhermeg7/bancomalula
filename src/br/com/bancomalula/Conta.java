package br.com.bancomalula;

/**
 * 
 * @author Guilherme Giroldo
 *
 */
public class Conta {

	public static double saldoDoBanco;

	// atributos
	private int numero;
	private double saldo;
	private String senha;
	private Cliente cliente;

	// construtor
	public Conta(int numero, double saldo, String senha, Cliente cliente) {

		this.numero = numero;
		this.saldo = saldo;
		this.senha = senha;
		this.cliente = cliente;
	}

	// Getters N' Setters

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public double getSaldo() {
		return saldo;
	}

	public void setSaldo(double saldo) {
		this.saldo = saldo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	// methods
	public void exibeSaldo() {

		System.out.println(cliente.getNome() + " Seu saldo � de R$ " + this.getSaldo());

	}

	public void saca(double valor) {
		this.saldo -= valor;
		Conta.saldoDoBanco -= valor;
	}

	public void deposita(double valor) {
		this.saldo += valor;
		Conta.saldoDoBanco += valor;
	}

	public void transferePara(Conta destino, double valor) {

		this.saca(valor);
		destino.deposita(valor);

	}

}
