package br.com.bancomalula;

/**
 * 
 * @author Guilherme Giroldo
 *
 */
public class BancoTeste {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Conta.saldoDoBanco = 2_000_000_000;

		
		Conta conta = new Corrente(12, 25000, "123654", new Cliente("Winstons", "123654", "987456887", "Churchill@Winston.com", "87458745", Sexo.MASCULINO));

		System.out.println("DADOS DA CONTA");
		System.out.println("N� " + conta.getNumero());
		System.out.println("Saldo: " + conta.getSaldo());
		System.out.println("Titular" + conta.getCliente().getNome());
		System.out.println("RG " + conta.getCliente().getRg());
		System.out.println("CPF " + conta.getCliente().getCpf());
	    
	}
}
